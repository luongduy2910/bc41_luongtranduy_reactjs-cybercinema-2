import React, { Component } from 'react';
import { connect } from 'react-redux';
import { formatter } from './ForMatter';

class PickedSeat extends Component {
    renderTongTien = () => {
        let tongTien = 0 ; 
        this.props.pickedseat.map(seat => {
            return tongTien += seat.gia ; 
        })
        return formatter.format(tongTien) ; 
    }
    render() {
        return (
            <div className='container p-5'>
                <h2>Danh sách ghế đã đặt</h2>
                <div style={{display : 'flex' , alignItems : 'center'}} className='mb-3'>
                    <span style={{width : '30px' , height : '30px' , display : 'inline-block'}} className='bg-light mr-2'></span>
                    <span style={{fontWeight : "bolder"}} className='text-light'>Ghế chưa đặt</span>
                </div>
                <div style={{display : 'flex' , alignItems : 'center'}} className='mb-3'>
                    <span style={{width : '30px' , height : '30px' , display : 'inline-block'}} className='bg-danger mr-2'></span>
                    <span style={{fontWeight : "bolder"}} className='text-danger'>Ghế đã đặt</span>
                </div>
                <div style={{display : 'flex' , alignItems : 'center'}} className='mb-5'>
                    <span style={{width : '30px' , height : '30px' , display : 'inline-block'}} className='bg-success mr-2'></span>
                    <span style={{fontWeight : "bolder"}} className='text-success'>Ghế đang chọn</span>
                </div>
                <table className="table table-dark table-inverse text-white">
                    <thead className="thead-inverse">
                        <tr>
                            <th>Số ghế</th>
                            <th>Giá</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            {this.props.pickedseat.map((seat , index)=> {
                                let {soGhe , gia } = seat
                                return (
                                    <tr key={index}>
                                        <td>{soGhe}</td>
                                        <td>{formatter.format(gia)}</td>
                                        <td><button onClick={() => {
                                            this.props.handleDeleteSeat(soGhe)
                                        }} className="btn btn-danger btn-sm">Delete</button></td>
                                    </tr>
                                )
                            })}
                            <tr>
                                <td>Tổng tiền</td>
                                <td>{this.renderTongTien()}</td>
                            </tr>
                        </tbody>
                </table>
            </div>
        );
    }
}

let mapStateToProps = (state) =>{
    return {pickedseat : state.seatReducer.pickedseat}
}

let mapDispatchToProps = (dispatch) =>{
    return {
        handleDeleteSeat : (soGhe) => {
            let action = {
                type : 'DELETE_SEAT' , 
                soGhe 
            }
            dispatch(action) ; 
        }
    }
}

export default connect(mapStateToProps , mapDispatchToProps)(PickedSeat);