import React, { Component } from 'react';
import PickedSeat from './PickedSeat';
import RowSeat from './RowSeat';

class CyberCinema extends Component {
    render() {
        return (
            <div className='row'>
                <div className="col-8">
                    <RowSeat/>
                </div>
                <div className='col-4'>
                    <PickedSeat/>
                </div>
            </div>
        );
    }
}

export default CyberCinema;