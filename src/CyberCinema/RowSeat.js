import React, { Component } from 'react';
import { stateGhe } from './stateGhe';
import { connect } from 'react-redux';

class RowSeat extends Component {
    renderRowSeat = () => {
        return this.props.dataSeat.map((rowSeat , index)=> {
            let {hang , danhSachGhe} = rowSeat
            return (
                <div key={index} className='mb-4'>
                    <span style={{color : 'orangered' , fontWeight : '900' , fontSize : '20px'}} className='mr-5'>{hang}</span>
                    {danhSachGhe.map((ghe ,index) => {
                        return <span key={index} onClick={() => {
                            this.props.handlePickSeat(ghe) 
                        }} style={{cursor : 'pointer'}} className={stateGhe(ghe.daDat)}>{ghe.soGhe}</span>
                    })}
                </div>
            );
        })
    }
    render() {
        return (
            <div className='container p-5'>
                <h2 className='text-center text-danger mb-5'>Danh sách ghế ngồi</h2>
                {this.renderRowSeat()}
            </div>
        );
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        handlePickSeat : (ghe) => {
            const action =  {
                type : "PICK_SEAT" , 
                ghe 
            }
            dispatch(action) ; 
        }
    }
}

let mapStateToProps = (state) => {
    return {dataSeat : state.seatReducer.dataSeat}
}

export default connect(mapStateToProps , mapDispatchToProps)(RowSeat);