import './App.css';
import CyberCinema from './CyberCinema/CyberCinema';
function App() {
  return (
    <div className='cyber__cinema'>
      <CyberCinema/>
    </div>
  );
}

export default App;
