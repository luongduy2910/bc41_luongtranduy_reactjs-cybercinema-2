import { dataSeat } from "../../dataSeat"
import Toastify from 'toastify-js'
let initialState = {
    dataSeat : dataSeat , 
    pickedseat : [] , 
}

export const seatReducer = (state = initialState , action) => {
    switch (action.type) {
        case "PICK_SEAT" : {
            if (action.ghe.daDat === false) {
                let pickedseatClone = [...state.pickedseat] ; 
                let dataSeatClone = [...state.dataSeat] ; 
                let index = pickedseatClone.findIndex(ghe => ghe.soGhe === action.ghe.soGhe) ; 
                if (index === -1) {
                    pickedseatClone.push(action.ghe) ; 
                    Toastify({
                        text: `Chọn ghế ${action.ghe.soGhe} thành công` ,
                        duration: 3000
                        
                        }).showToast();
                    dataSeatClone.forEach(ghe1 => {
                        let index =  ghe1.danhSachGhe.findIndex(ghe2 => ghe2.soGhe === action.ghe.soGhe) ;
                        if (index !== -1) {
                            ghe1.danhSachGhe[index].daDat = "dangDat" ;
                        }
                        state.dataSeat = dataSeatClone ; 
                        state.pickedseat =pickedseatClone ; 
                        return {...state} ; 
                        
                    })
                }
                else {
                    Toastify({
                        text: `Bạn đã chọn ghế ${action.ghe.soGhe} rồi. Vui lòng chọn thêm` ,
                        duration: 3000
                        
                        }).showToast();
                }
                
            } else if (action.ghe.daDat === "dangDat") {
                Toastify({
                    text: `Bạn đang chọn ghế ${action.ghe.soGhe}. Vui lòng chọn thêm ghế khác` ,
                    duration: 3000
                    
                    }).showToast();
            }
            else {
                Toastify({
                    text: `ghế ${action.ghe.soGhe} đã có người chọn . Vui lòng chọn ghế khác` ,
                    duration: 3000
                    
                    }).showToast();
            }
        }
        case "DELETE_SEAT" : {
            let pickedseatClone = [...state.pickedseat] ; 
            let dataSeatClone = [...state.dataSeat] ; 
            let filteredSeat = pickedseatClone.filter(ghe => ghe.soGhe !== action.soGhe)
            dataSeatClone.forEach(ghe1 => {
                let index =  ghe1.danhSachGhe.findIndex(ghe2 => ghe2.soGhe === action.soGhe) ;
                if (index !== -1) {
                    ghe1.danhSachGhe[index].daDat = false ;
                }                       
                state.dataSeat = dataSeatClone ; 
                state.pickedseat = filteredSeat ; 
                return {...state} ; 
            })
        }
    }
    return {...state} 
}